import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from 'controller/Store';
import ChatContainer from 'view/container/Chat';
import { Loading } from 'view/factory'

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={<Loading />} persistor={persistor}>
        <ChatContainer />
      </PersistGate>
    </Provider>
  );
}

export default App;
