
import { combineReducers } from "redux";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import chat from 'controller/reducers/chatReducer';

const appReducer = combineReducers({
    chat,
});

const rootReducer = (state, action) => {
    if (action.type === "LOGOUT") state = undefined;
    return appReducer(state, action);
}

export default persistReducer({
    key: 'root',
    storage: storage,
    whitelist: ['chat']
}, rootReducer);
