
import { fetchChat, sendMessage } from 'model/graphql/chat';

export function fetchChatAction(chatId) {
    return {
        type: "FETCH_CHATS",
        payload: fetchChat(chatId),
    }
}

export function sendMessageAction(user, chatId, content) {
    return {
        type: "SEND_MESSAGE",
        payload: sendMessage(user, chatId, content),
    }
}