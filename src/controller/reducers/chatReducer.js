export default function reducer(state = {
    chats: {},
    loading: false,
    success: false,
    error: false,
}, action) {
    switch (action.type) {

        case "NEW_MESSAGE": {
            console.log(`New Message: `, action.payload.data);
            const { chat } = action.payload.data;
            const chats = JSON.parse(JSON.stringify(state.chats));
            chats[`${chat.id}`] = chat;
            return {
                ...state,
                chats,
            }
        }

        case "FETCH_CHATS_PENDING": {
            console.log(`Fetching Chats`);
            return {
                ...state,
                loading: true,
                success: false,
                error: false,
            }
        }

        case "FETCH_CHATS_FULFILLED": {
            console.log(`Chats Fetched: `, action.payload.data);
            const { chat } = action.payload.data;
            const chats = JSON.parse(JSON.stringify(state.chats));
            chats[`${chat.id}`] = chat;
            return {
                ...state,
                chats,
                loading: false,
                success: true,
                error: false,
            }
        }

        case "FETCH_CHATS_REJECTED": {
            console.log(`Failed to Fetch Chats: `, action.payload);
            return {
                ...state,
                loading: false,
                success: false,
                error: true,
            }
        }

        default: return state;

    }
}