
import { store } from 'controller/Store';
import client from "model/graphql";
import fetchChatSchema from './schema/fetchChat';
import sendMessageSchema from './schema/sendMessage';
import subscribeSchema from './schema/subscribe';

export function fetchChat(chatId) {
  return client.query({
    query: fetchChatSchema,
    variables: { chatId },
  });
}

export function sendMessage(user, chatId, content) {
  return client.mutate({
    mutation: sendMessageSchema,
    variables: { user, chatId, content },
  });
}

export function subscribe(chatId, user) {
  return client.subscribe({
    query: subscribeSchema,
    variables: { id: chatId, user }
  }).subscribe({
    next(data) {
      store.dispatch({
        type: 'NEW_MESSAGE',
        payload: data,
      });
    }
  })
}