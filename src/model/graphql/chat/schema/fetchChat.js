import { gql } from "apollo-boost";

const schema = gql`
query($chatId: String) {
  chat(chatId: $chatId) {
    id
    messages {
      id
      from
      content
      date
    }
  }
}
`;

export default schema;