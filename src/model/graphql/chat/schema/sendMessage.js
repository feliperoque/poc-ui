import { gql } from "apollo-boost";

const schema = gql`
mutation($user: String, $chatId: String, $content: String) {
  sendMessage(user: $user, chatId: $chatId, content: $content) {
    success
  }
}
`;

export default schema;