import { gql } from "apollo-boost";

const schema = gql`
subscription($id: String, $user: String) {
  chat(id: $id, user: $user) {
    id
    messages {
      id
      from
      content
      date
    }
  }
}
`;

export default schema;