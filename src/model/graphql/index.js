import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { WebSocketLink } from 'apollo-link-ws';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { getSearchParameters } from 'model/common/url';

const port = getSearchParameters().port || 3001;

const hasSubscriptionOperation = ({ query: { definitions } }) =>
  definitions.some(
    ({ kind, operation }) =>
      kind === 'OperationDefinition' && operation === 'subscription',
  )

const link = ApolloLink.split(
  hasSubscriptionOperation,
  new WebSocketLink(
    new SubscriptionClient(`ws://localhost:${port}/graphql`, {
      reconnect: true,
    }),
  ),
  createHttpLink({
    uri: `http://localhost:${port}/graphql`,
  }),
)

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(window.__APOLLO_STATE__),
})

export default client;