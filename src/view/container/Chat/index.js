import React from 'react';
import { connect } from 'react-redux';
import { subscribe } from 'model/graphql/chat';
import { fetchChatAction, sendMessageAction } from 'controller/actions/chatAction';
import { getSearchParameters } from 'model/common/url';
import ChatView from 'view/container/Chat/view';

class ChatContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    const user = getSearchParameters().user || "Guest";
    const chatId = getSearchParameters().chatId || "general";
    subscribe(chatId, user);
  }

  componentDidMount() {
    const chatId = getSearchParameters().chatId || "general";
    this.props.fetchChat(chatId);
  }

  onSend = (message) => {
    const user = getSearchParameters().user || "Guest";
    const chatId = getSearchParameters().chatId || "general";
    this.props.sendMessage(user, chatId, message);
  }

  render() {
    return <ChatView
      messages={this.props.messages}
      onSend={this.onSend}
    />
  }
}

function mapStateToProps(state) {
  const chatId = getSearchParameters().chatId || "general";
  const chat = state.chat.chats[`${chatId}`] || {};
  return {
    messages: chat.messages || [],
    loading: state.chat.loading,
    success: state.chat.success,
    error: state.chat.error,
  }
}

function mapDispatchToProps(dispatch) {
  return ({
    fetchChat: chatId => dispatch(fetchChatAction(chatId)),
    sendMessage: (user, chatId, content) => dispatch(sendMessageAction(user, chatId, content)),
  });
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatContainer);