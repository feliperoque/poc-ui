import React from 'react';
import { View, Text, Input, Image } from 'view/factory';
import { timeSince } from 'model/common/date';
import { getSearchParameters } from 'model/common/url';

const user = getSearchParameters().user;
const sendIcon = require('assets/send.svg');

export default class ChatView extends React.PureComponent {
  state = {
    message: '',
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom = () => {
    this.endOfMessages.scrollIntoView({ behavior: "smooth" });
  }

  onChange = (event) => {
    this.setState({ message: event.target.value });
  }

  onKeyDown = (key) => {
    if (key.keyCode === 13) {
      if (!this.state.message) return;
      this.props.onSend(this.state.message);
      this.setState({ message: '' });
    }
  }

  renderHeader() {
    return (
      <Text width={'100%'} align={'center'} h1 bold>myG Chat POC</Text>
    );
  }

  renderMessages() {
    return (
      <View
        color={'#F0F0F0'}
        scroll
        mVertical={16}
        mHorizontal={8}
        padding={4}
        radius={4}
        border={{ width: 1, color: '#333' }}
        flex
      >
        {this.props.messages.map(message => this.renderMessage(message))}
        <View ref={element => this.endOfMessages = element} />
      </View>
    );
  }

  renderMessage(message) {
    if (message.from === "MYQ_SERVER") return this.renderServerMessage(message);
    return (
      <View key={message.id}
        color={message.from === user ? '#DAFADA' : '#FAFAFA'}
        radius={4}
        width={'fit-content'}
        selfEnd={message.from === user}
        shadow row
        spaceBetween
        center
        padding={8} mVertical={4} mHorizontal={4}
      >
        <View>
          <Text breakline>{message.content}</Text>
        </View>
        <View minWidth={'fit-content'} mLeft={16}>
          <Text color={'#606060'} body align={'right'} mBottom={4}>From {message.from === user ? "You" : message.from}</Text>
          <Text color={'#606060'} caption align={'right'}>{timeSince(new Date(message.date))} ago</Text>
        </View>
      </View>
    );
  }

  renderServerMessage(message) {
    return (
      <View key={message.id}
        selfEnd row
        spaceBetween
        center
        mVertical={4} mHorizontal={4}
      >
        <View>
          <Text color={'#606060'} caption>{message.content}</Text>
        </View>
      </View>
    );
  }

  renderInput() {
    return (
      <View width={'100%'}>
        <View mHorizontal={6} flex spaceBetween row center>
          <Input
            value={this.state.message}
            onChange={this.onChange}
            onKeyDown={this.onKeyDown}
          />
          <View touchable>
            <Image src={sendIcon} mLeft={16} size={40} />
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View flex width={'100%'} fullscreen center color={'#F0F0F0'}>
        <View flex width={'100%'} maxWidth={600} maxHeight={600} padding={16} shadow color={'#FAFAFA'}>
          {this.renderHeader()}
          {this.renderMessages()}
          {this.renderInput()}
        </View>
      </View>
    );
  }
}